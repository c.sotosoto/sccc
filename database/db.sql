USE sociedad_rsk;

--USERS TABLES
CREATE TABLE users(
    id INT(11) NOT NULL,
    nombre VARCHAR(150) NOT NULL,
    apellido VARCHAR(150) NOT NULL,
    email VARCHAR(255) NOT NULL,
    email2 VARCHAR(255) NOT NULL,
    description TEXT,
    create_at timestamp NOT NULL DEFAULT current_timestamp
);

ALTER TABLE users
    ADD PRIMARY KEY (id);

ALTER TABLE users
    MODIFY id INT(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 2;

DESCRIBE users;