const express = require('express');
const router  = express.Router();

const pool = require('../database'); //referencia a la conexion de la bbdd

router.get('/add', (req, res) => {
    res.render('links/add');
});

router.post('/add', async (req, res) =>{
    const { nombre, apellido, email, email2, description } = req.body;
    const newLink = {
        nombre,
        apellido,
        email,
        email2,
        description
    };
    await pool.query('INSERT INTO users set ?', [newLink]);
    // res.send('Debera esperar que un administrador apruebe su ingreso');
    res.render('links/list');
});

router.get('/', async (req, res) =>{
    // const links = await pool.query('SELECT * FROM users');
    res.render('links/list');
});
module.exports = router;