module.exports = {
    
    database: {
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        database: process.env.DB_NAME,
        password: process.env.DB_PASS,
        socketPath: `/cloudsql/${process.env.INSTANCE_CONNECTION_NAME}`,
    }
}